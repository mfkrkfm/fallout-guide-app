//
//  UIColor.swift
//  Eateries
//
//  Created by Игорь Зацеркляный on 12.04.2020.
//  Copyright © 2020 Igor Zatserklyanyy. All rights reserved.
//

import UIKit

extension UIColor {
    static let aqua = UIColor(named: "aqua")!
}

