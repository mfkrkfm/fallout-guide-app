//
//  TitleViewController.swift
//  Eateries
//
//  Created by Игорь Зацеркляный on 16.05.2020.
//  Copyright © 2020 Igor Zatserklyanyy. All rights reserved.
//

import UIKit

class TitleViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var titleString: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    
        titleLabel.text = titleString
    }
    

}
