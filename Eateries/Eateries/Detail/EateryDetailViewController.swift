//
//  EateryDetailViewController.swift
//  Eateries
//
//  Created by Igor Zatserklyanyy on 8/21/19.
//  Copyright © 2019 Igor Zatserklyanyy. All rights reserved.
//

import UIKit

class EateryDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var rateButton: UIButton!
    @IBOutlet weak var mapButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var imageView: UIImageView!
    var faction: Faction?
    
    @IBAction func unwindSegue(segue: UIStoryboardSegue){
        guard let svc = segue.source as? RateViewController else {return}
        guard let rating = svc.factRating else {return}
        rateButton.setImage(UIImage(named: rating), for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.hidesBarsOnSwipe = false
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        rateButton.layer.cornerRadius = 5
//        rateButton.layer.borderWidth = 1
//        rateButton.layer.borderColor = UIColor.white.cgColor
        
        tableView.estimatedRowHeight = 38
        tableView.rowHeight = UITableView.automaticDimension
        imageView.image = UIImage(data: faction!.image! as Data)
        
        //        tableView.backgroundColor = #colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1)
        //        tableView.separatorColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        title = faction!.name
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as!
        EateryDetailTableViewCell
        switch indexPath.row {
        case 0:
            cell.keyLabel.text = "Name:"
            cell.valueLabel.text = faction!.name
        case 1:
            cell.keyLabel.text = "Leader:"
            cell.valueLabel.text = faction!.type
        case 2:
            cell.keyLabel.text = "Location:"
            cell.valueLabel.text = faction!.location
        case 3:
            cell.keyLabel.text = "Membership:"
            cell.valueLabel.text = faction!.factionJoined ? "Yes" : "No"
        default:
            break
        }
        cell.backgroundColor = UIColor.clear
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "mapSegue"{
            let dvc = segue.destination as! MapViewController
            dvc.faction = self.faction
        }
    }


}
