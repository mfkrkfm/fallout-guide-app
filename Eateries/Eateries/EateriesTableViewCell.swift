//
//  EateriesTableViewCell.swift
//  Eateries
//
//  Created by Igor Zatserklyanyy on 8/14/19.
//  Copyright © 2019 Igor Zatserklyanyy. All rights reserved.
//

import UIKit

protocol EateriesCellDelegate: class {
    func openTitleController(with title: String)
}

class EateriesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    
    weak var delegate: EateriesCellDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

 @IBAction func testAction(_ sender: Any) {
    let title = nameLabel.text ?? ""
    delegate?.openTitleController(with: title)
    
 }

}
