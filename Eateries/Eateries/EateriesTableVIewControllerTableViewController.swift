//
//  EateriesTableVIewControllerTableViewController.swift
//  Eateries
//
//  Created by Igor Zatserklyanyy on 7/27/19.
//  Copyright © 2019 Igor Zatserklyanyy. All rights reserved.
//

import UIKit
import CoreData

class EateriesTableVIewControllerTableViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    
    var fetchResultsController: NSFetchedResultsController<Faction>!
    var searchController: UISearchController!
    var filteredResultArray: [Faction] = []
    var factions: [Faction] = []
    //        Faction(name: "The Brotherhood of Steel", type: "Nolan McNamara(Hidden Valley)", location: "Unknown", image: "BrotherhoodOfSteelReputation.png", factionJoined: false),
    //        Faction(name: "New California Republic", type: "General Oliver(Hoover Dam)", location:
    //            "Hoover Dam Access Rd Willow Beach, AZ 86445 United States", image: "NCRReputation.png", factionJoined: false),
    //        Faction(name: "Caesar's Legion", type: "Edward Sallow, also known as Caesar(Fortification Hill)", location:
    //            "Willow Beach, AZ  86445 United States", image: "CaesarLegionReputation.png", factionJoined: false),
    //        Faction(name: "Followers of the Apocalypse", type: "Julie Farkas(Old Mormon Fort)", location: "500 E Washington Ave, at Las Vegas Blvd. North, Лас-Вегас, NV 89101-1000", image: "FollowersApocalypseReputation.png", factionJoined: false),
    //        Faction(name: "The Great Khans", type: "Papa Khan(Red Rock)", location:
    //            "3205 State Hwy 159 Las Vegas, NV  89166 United States", image: "GreatKhansNewVegas.png", factionJoined: false),
    //        Faction(name: "Powder Gangers", type: "Eddie(NCR Correctional Facility)", location:
    //            "22010 Cold Creek Rd Indian Springs, NV 89070 United States", image: "PowderNewVegas.png", factionJoined: false),
    //        Faction(name: "Boomers", type: "Mother Pearl(Nellis Aviabase)", location:
    //            "Nellis Air Force Base Лас-Вегас, NV United States", image: "BoomersReputation.png", factionJoined: false),
    //        Faction(name: "The White Glove Society", type: "Marjorie(New Vegas Strip)", location:
    //            "3647–3649 Las Vegas Blvd S Las Vegas, NV  89109 United States", image: "WhiteGloveSociety.png", factionJoined: false)
    //        ]
    
    
    
    let steel = "The Brotherhood of Steel was founded by Roger Maxson, a captain in the United States Army. Led by Colonel Robert Spindel, Maxson was part of a team sent on January 3, 2076 to monitor progress at a West-Tek facility in California, which was conducting research on behalf of the American government. On January 7, 2077, all West-Tek research and personnel – Maxson and his team included – were relocated to the newly constructed Mariposa Military Base in an effort to enhance security."
    let ncr = "New California (officially the New California Republic, often abbreviated to NCR) is a large, democratic federation of states with a population of well over 700,000 based in California, with holdings in Nevada, Mexico (Baja California) and along the Colorado River. The NCR emphasizes and claims to support a myriad of old-world values, such as democracy, personal liberty, and the rule of law. It also strives to restore general order to the wasteland, the improvement and development of infrastructure and economic systems, and a basic common peace between the people."
    let legion = "Caesar's Legion is an autocratic, traditionalist, imperialistic slaver society and totalitarian dictatorship. Founded in 2247 by Edward Sallow (also known as Caesar) and Joshua Graham, it is based on the ancient Roman Empire. Its legionaries are a well organized, culturally insular fighting force that mainly operate east of the Colorado River and the Grand Canyon, in the former state of Arizona. Ever pushing west, their capital is the city of Flagstaff. Caesar's Legion is comprised mostly of reconditioned tribes and their descendants. The Legion's symbol is a golden bull on a red field, which is derived from Julius Caesar's standard for the Tenth Twin Legion."
    let followers = "The Followers of the Apocalypse, or simply the Followers, are a faction based in New California and have established their presence in the Mojave Wasteland. Their goal is to tend to the inhabitants of the wasteland, as well as to ensure that the horrors of the Great War are never to be repeated. To that end, they serve as keepers of knowledge, a position which provides them with the skills they need to carry out their mission."
    let khans = "The Great Khans are the only truly organized band of raiders in the Mojave and, as befits their warrior culture, men and women both can hold their own in a fight, whether in a brawl or a shootout. As a tribe however, the Great Khans are but a shadow of their former selves. Both their numbers and morale have been ravaged by a series of massacres and displacements. From their rocky stronghold at Red Rock Canyon, they make a living by drug-trafficking and the occasional raid or salvage find."
    let powderGangers = "The NCR Correctional Facility is the dominant location of the central Mojave Wasteland. In the distant past, it was the Jean Conservancy, a low-security all-female prison. Under recent NCR control, it was used to house prisoners on work release. The prisoners worked the railroad parallel to the Long 15 under NCR trooper supervision, maintaining the vital land link between the Boneyard and New Vegas. Eventually, prison guards (NCR troopers) were pulled away to run Colorado River border patrol. When the guard staff was low, the prisoners executed a daring and violent coup."
    let boomers = "A group of Vault 34’s dissidents struck out on their own and started using Nellis as a base. Over a long period of time, they eventually decided to call Nellis their home. They learned a great deal of information from the records at Nellis and used the information to open the weapons storehouses at unknown locations called Area 2 and the Hawthorne Army Depot. The result of their efforts was an enormous stockpile of heavy ordnance, artillery, and small nuclear weapons."
    let whiteGlove = "The White Glove Society is one of the Three Families responsible for the operation and maintenance of the casinos on the New Vegas Strip, specifically the Ultra-Luxe in 2281. The Ultra-Luxe is an establishment of incredible refinement that delicately conceals its operators’ terrible past; a past which the tribe has been at pains to remove from public record or knowledge to the point of fanaticism. Run by the White Glove Society, the Ultra-Luxe pampers its clients and provides the Strip’s most elite casino experience. A dress code is strictly enforced. All of the staff and family members are well-dressed, well-spoken, and well-behaved. "
    
    @IBAction func close(segue: UIStoryboardSegue){} 
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.hidesBarsOnSwipe = true
        
        
        //        let fetchRequest: NSFetchRequest<Faction> = Faction.fetchRequest()
        //        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        //        fetchRequest.sortDescriptors = [sortDescriptor]
        //        if let context = (UIApplication.shared.delegate as? AppDelegate)?.coreDataStack.persistentContainer.viewContext{
        //            fetchResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        //
        //            do {
        //                try fetchResultsController.performFetch()
        //                factions = fetchResultsController.fetchedObjects!
        //            } catch let error as NSError {
        //                print(error.localizedDescription)
        //            }
        //        }
        //        func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        //
        //            switch type {
        //            case .insert: guard let indexPath = newIndexPath else { break }
        //            tableView.insertRows(at: [indexPath], with: .fade)
        //            case .delete: guard let indexPath = indexPath else { break }
        //            tableView.deleteRows(at: [indexPath], with: .fade)
        //            case .update: guard let indexPath = indexPath else { break }
        //            tableView.reloadRows(at: [indexPath], with: .fade)
        //            default:
        //                tableView.reloadData()
        //            }
        //            factions = controller.fetchedObjects as! [Faction]
        //            tableView.reloadData()
        //
        //        }
    }
    
    func filterContentFor(searchText text: String){
        filteredResultArray = factions.filter{ (faction) -> Bool in
            return (faction.name?.lowercased().contains(text.lowercased()))!
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        tableView.tableHeaderView = searchController.searchBar
        searchController.searchBar.delegate = self
        searchController.searchBar.barTintColor = #colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1)
        searchController.searchBar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        definesPresentationContext = true
        
        tableView.estimatedRowHeight = 85
        tableView.rowHeight = UITableView.automaticDimension
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        let fetchRequest: NSFetchRequest<Faction> = Faction.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.coreDataStack.persistentContainer.viewContext{
            fetchResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
            
            do {
                try fetchResultsController.performFetch()
                factions = fetchResultsController.fetchedObjects!
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>){
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert: guard let indexPath = newIndexPath else { break }
        tableView.insertRows(at: [indexPath], with: .fade)
        case .delete: guard let indexPath = indexPath else { break }
        tableView.deleteRows(at: [indexPath], with: .fade)
        case .update: guard let indexPath = indexPath else { break }
        tableView.reloadRows(at: [indexPath], with: .fade)
        default:
            tableView.reloadData()
        }
        factions = controller.fetchedObjects as! [Faction]
        
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>){
        tableView.endUpdates()
    }
    
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != ""{
            return filteredResultArray.count
        }
        return factions.count
    }
    
    func factionToDisplayAt(indexPath: IndexPath) -> Faction{
        let faction: Faction
        if searchController.isActive && searchController.searchBar.text != ""{
            faction = filteredResultArray[indexPath.row]
        } else {
            faction = factions[indexPath.row]
        }
        return faction
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as!
        EateriesTableViewCell
        
        let faction = factionToDisplayAt(indexPath: indexPath)
        //        cell.thumbnailImageView.layer.cornerRadius = 32.5
        //        cell.thumbnailImageView.clipsToBounds = true
        cell.thumbnailImageView.image = UIImage(data: faction.image! as Data)
        cell.nameLabel.text = faction.name
        cell.locationLabel.text = faction.location
        cell.typeLabel.text = faction.type
        
        cell.delegate = self
        
        //        if self.factionJoined[indexPath.row]{
        //            cell.accessoryType = .checkmark
        //        } else {
        //            cell.accessoryType = .none
        //        }
        cell.accessoryType = faction.factionJoined ? .checkmark : .none
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let invite = UITableViewRowAction (style: .default, title: "invite"){(action, indexPath) in
            
            let defaultText  = "Join me and " + self.factions[indexPath.row].name!
            if let image = UIImage(data: self.factions[indexPath.row].image! as Data){
                let activityController = UIActivityViewController(activityItems: [defaultText, image], applicationActivities:  nil)
                self.present(activityController, animated: true, completion: nil)
            }
        }
        let delete = UITableViewRowAction(style: .default, title: "Удалить") { (action, indexPath) in
            self.factions.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            
            if let context = (UIApplication.shared.delegate as? AppDelegate)?.coreDataStack.persistentContainer.viewContext{
                
                let objectToDelete = self.fetchResultsController.object(at: indexPath)
                context.delete(objectToDelete)
                
                do {
                    try context.save()
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
        
        delete.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        invite.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        return[invite, delete]
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailSegue"{
            if let indexPath = tableView.indexPathForSelectedRow {
                let dvc = segue.destination as! EateryDetailViewController
                dvc.faction = self.factions[indexPath.row]
            }
        }
    }
    
    
    func createVC(with title: String) {
        guard
            let vc = storyboard?.instantiateViewController(withIdentifier: "TitleViewController") as? TitleViewController
            else {
                return
        }
        
        vc.titleString = title
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

extension EateriesTableVIewControllerTableViewController: EateriesCellDelegate {
    func openTitleController(with title: String) {
        createVC(with: title)
    }
    
    
}
extension EateriesTableVIewControllerTableViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContentFor(searchText: searchController.searchBar.text!)
        tableView.reloadData()
    }
}
extension EateriesTableVIewControllerTableViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if searchBar.text == ""{
            navigationController?.hidesBarsOnSwipe = false
        }
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar){
        navigationController?.hidesBarsOnSwipe = true
    }
}
