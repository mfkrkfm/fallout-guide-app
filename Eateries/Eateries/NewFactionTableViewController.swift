//
//  NewFactionTableViewController.swift
//  Eateries
//
//  Created by Игорь Зацеркляный on 24.02.2020.
//  Copyright © 2020 Igor Zatserklyanyy. All rights reserved.
//

import UIKit

class NewFactionTableViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    
    
    @IBOutlet weak var noButton: UIButton!
    @IBOutlet weak var yesButton: UIButton!
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var leaderTextField: UITextField!
    @IBOutlet weak var locationTextField: UITextField!
    var factionJoined = false
    
    @IBAction func toggleIsVisitedPressed(_ sender: UIButton) {
        if sender == yesButton{
            sender.backgroundColor = #colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1)
            noButton.backgroundColor = .gray
            factionJoined = true
        } else {
            sender.backgroundColor = .red
            yesButton.backgroundColor = .gray
            factionJoined = false
        }
    }

    @IBAction func saveButtonPressed(_ sender: UIBarButtonItem) {
        if nameTextField.text == "" || locationTextField.text == "" || leaderTextField.text == "" {
            let alert = UIAlertController(title: "Error", message: "Fill all Fields", preferredStyle: .alert)
            self.present(alert, animated: true, completion: {
                alert.view.superview?.isUserInteractionEnabled = true
                alert.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTapOutside)))
            })
            
        } else {
            
            if let context = (UIApplication.shared.delegate as? AppDelegate)?.coreDataStack.persistentContainer.viewContext{
                
                let faction = Faction(context: context)
                faction.name = nameTextField.text
                faction.location = locationTextField.text
                faction.type = leaderTextField.text
                faction.factionJoined = factionJoined
                if let image = imageView.image {
                    faction.image = image.pngData()! as Data
                }
                
                do{
                    try context.save()
                    print("сохранение удалось")
                } catch let error as NSError {
                    print("Не удалось сохранить данные \(error), \(error.userInfo)")
                }
                
                
            }
            
            performSegue(withIdentifier: "unwindSegueFromNewFaction", sender: self)
        }
    }
    
    @IBOutlet weak var imageView: UIImageView!
    
    @objc func dismissOnTapOutside(){
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        yesButton.backgroundColor = #colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1)
        noButton.backgroundColor = .red
        view.backgroundColor = .aqua
        
        title = "New Faction"
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imageView.image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        dismiss(animated: true, completion: nil)
    }
    
    func chooseImagePickerAction(source: UIImagePickerController.SourceType){
        if UIImagePickerController.isSourceTypeAvailable(source){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = source
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    // MARK: - Table view data source
    
    
    // MARK: - Table View Delegate
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    fileprivate func showAlertController() {
        let alertController = UIAlertController(title: "Pick photo from:", message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            self.chooseImagePickerAction(source: .camera)
        })
        let photoLibAction = UIAlertAction(title: "Library", style: .default, handler: { (action) in
            self.chooseImagePickerAction(source: .photoLibrary)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cameraAction)
        alertController.addAction(photoLibAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            showAlertController()
        case 1:
            nameTextField.becomeFirstResponder()
        case 2:
            locationTextField.becomeFirstResponder()
        case 3:
            leaderTextField.becomeFirstResponder()

        default:
            break
        }
        
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

extension NewFactionTableViewController: UITextFieldDelegate {
    
}
